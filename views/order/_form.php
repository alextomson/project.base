<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (!$create): ?>
        <?= $form->field($model, 'status')->widget(Select2::classname(), [
            'data' => $model::getStatuses(),
            'options' => ['placeholder' => 'Select a status ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    <?php endif; ?>


    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if (!$create): ?>
        <p>
            <?= Html::a('Create Order Product', ['order-product/create?order_id=' . $model->id], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'product_id',
                    'value' => function ($model) {
                        return $model->getProductName();
                    }
                ],
                'count',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}',

                    'buttons' => [

                        'update' => function ($url, $model) {

                            $url = Url::to(['order-product/update', 'id' => $model->id]);

                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            $url = Url::to(['order-product/delete', 'id' => $model->id]);
                            $options = [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];

                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        }
                    ]
                ],
            ],
        ]); ?>
    <?php endif; ?>

</div>
