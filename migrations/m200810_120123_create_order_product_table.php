<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order-product}}`.
 */
class m200810_120123_create_order_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order-product}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count' => $this->integer()->defaultValue(1),
        ]);

        $this->addForeignKey(
            'fk-order-product-order_id',
            'order-product',
            'order_id',
            'orders',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-product-product_id',
            'order-product',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-order-product-order_id',
            'order-product'
        );
        $this->dropForeignKey(
            'fk-order-product-product_id',
            'order-product'
        );
        $this->dropTable('{{%order-product}}');
    }
}
