<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $status
 * @property string $number
 *
 * @property OrderProduct[] $orderProducts
 */
class Order extends \yii\db\ActiveRecord
{
    public const DONE = 'Done';
    public const IN_PROCCESS = 'In proccess';
    public const CREATE = 'Create';
    public const FAIL = 'Fail';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['number'], 'required'],
            [['number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'number' => 'Number',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::CREATE,
            self::IN_PROCCESS,
            self::DONE,
            self::FAIL,
        ];
    }

    public function getOrderStatus()
    {
        return self::getStatuses()[$this->status];
    }

    /**
     * Gets query for [[OrderProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])
            ->viaTable('order-product', ['order_id' => 'id']);
    }
}
